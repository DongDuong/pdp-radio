+++
Description = "'Bầu trời ngàn sao lấp lánh / Lung linh ước vọng học trò / Mái trường long lanh mắt sáng / Ngời ngời ước vọng thầy cô…' Thầy cô, những người lái đò đưa học sinh tới bến đỗ của thành công. Người mang trong mình những trọng trách cao quý, nghề “trồng người”. Một mùa lá vàng rơi lại tới mang theo bao cái nhộn nhịp của ngày Nhà Giáo Việt Nam, ta dành trọn hôm nay để tôn vinh những người thầy, người cô đang và đã dạy ta nên người. Nhân ngày 20/11, câu lạc bộ PĐP Radio xin chúc các thầy, các cô luôn vui vẻ, hạnh phúc và thành công trong sự nghiệp đầy cao quý này. PĐP Radio nói chung và các bạn học sinh nói riêng dù có ở phương trời nào cũng mãi một lòng kính trọng tới những người lái đò đầy cao cả."
Date = 2021-11-20T20:25:00+07:00
PublishDate = 2021-11-20T20:25:00+07:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
podcast_file = "talktoradio-5.mp3" # the name of the podcast file, after the media prefix.
podcast_duration = "05:11"
#podcast_bytes = "" # the length of the episode in bytes
episode_image = "img/episode/default.png"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
#episode = ""
title = "Talk to Radio 5"
#subtitle = ""
images = ["img/episode/default-social.jpg"]
#hosts = [] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
#media_override # if you want to use a specific URL for the audio file
#truncate = ""
#upcoming = true # set to true if you want this to be listed as upcoming, etc, etc
#categories = []
#series = []
#tags = []
+++
