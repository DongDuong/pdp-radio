+++
Description = "Tình cảm tuổi học trò, quả thực là những ký ức thật khó phai trong tim mỗi người. Có một bóng hình, ở thanh xuân đầy nắng ấy, sẽ mãi là vầng ánh dương chói lóa mà chúng ta không thể nào quên..."
Date = 2021-08-21T20:00:00+07:00
PublishDate = 2021-08-21T20:00:00+07:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
podcast_file = "talktoradio-3.mp3" # the name of the podcast file, after the media prefix.
podcast_duration = "05:50"
#podcast_bytes = "" # the length of the episode in bytes
episode_image = "img/episode/talktoradio-3.jpg"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
#episode = ""
title = "Talk to Radio 3"
#subtitle = ""
images = ["img/episode/default-social.jpg"]
#hosts = [] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
#media_override # if you want to use a specific URL for the audio file
#truncate = ""
#upcoming = true # set to true if you want this to be listed as upcoming, etc, etc
#categories = []
#series = []
#tags = []
+++
