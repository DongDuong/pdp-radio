+++
Description = "Sự khác biệt giữa quá khứ, hiện tại và tương lai đôi khi chỉ là một ảo tưởng dai dẳng đến ngoan cố. Đừng đếm những gì bạn đã mất, hãy quý trọng những gì bạn đang có và lên kế hoạch cho những gì sẽ đạt được bởi quá khứ không bao giờ trở lại, nhưng tương lai có thể bù đắp cho mất mát..."
Date = 2020-05-25T21:24:20+07:00
PublishDate = 2020-05-25T21:24:20+07:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
podcast_file = "talktoradio-1.mp3" # the name of the podcast file, after the media prefix.
podcast_duration = "10:05"
#podcast_bytes = "" # the length of the episode in bytes
episode_image = "img/episode/talktoradio-1.png"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
#episode = ""
title = "Talk to Radio 1"
#subtitle = ""
images = ["img/episode/default-social.jpg"]
#hosts = [] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
#media_override # if you want to use a specific URL for the audio file
#truncate = ""
#upcoming = true # set to true if you want this to be listed as upcoming, etc, etc
#categories = []
#series = []
#tags = []
+++
