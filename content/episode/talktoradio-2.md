+++
Description = "'Ai trong chúng ta cũng xứng đáng được hưởng niềm hạnh phúc'.Và có thể, khi bạn vẫn đang trong nỗi nhớ thương một người, biết đâu bạn lại bỏ lỡ một tâm hồn đồng điệu với mình thì sao?"
Date = 2020-06-25T21:00:00+07:00
PublishDate = 2020-06-25T21:00:00+07:00 # this is the datetime for the when the epsiode was published. This will default to Date if it is not set. Example is "2016-04-25T04:09:45-05:00"
podcast_file = "talktoradio-2.mp3" # the name of the podcast file, after the media prefix.
podcast_duration = "04:00"
#podcast_bytes = "" # the length of the episode in bytes
episode_image = "img/episode/default.png"
#episode_banner = ""
#guests = [] # The names of your guests, based on the filename without extension.
#sponsors = []
#episode = ""
title = "Talk to Radio 2"
#subtitle = ""
images = ["img/episode/default-social.jpg"]
#hosts = [] # The names of your hosts, based on the filename without extension.
#aliases = ["/##"]
#youtube = ""
explicit = "no" # values are "yes" or "no"
#media_override # if you want to use a specific URL for the audio file
#truncate = ""
#upcoming = true # set to true if you want this to be listed as upcoming, etc, etc
#categories = []
#series = []
#tags = []
+++
